package com.jpmc.codechallenge.data.network

import com.google.gson.annotations.SerializedName

data class SchoolsResponse(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("school_name") val schoolName: String?,
    @SerializedName("phone_number") val phoneNumber: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("state_code") val stateCode: String?,
    @SerializedName("school_email") val schoolEmail: String?
)
