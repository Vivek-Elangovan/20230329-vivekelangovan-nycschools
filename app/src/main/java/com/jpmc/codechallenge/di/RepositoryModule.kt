package com.jpmc.codechallenge.di

import com.jpmc.codechallenge.data.NetworkSchoolSource
import com.jpmc.codechallenge.data.SchoolsRepositoryImpl
import com.jpmc.codechallenge.data.repository.SchoolsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun SchoolsRepository(dataSource: NetworkSchoolSource): SchoolsRepository = SchoolsRepositoryImpl(dataSource)
}