package com.jpmc.codechallenge.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    val dbn: String = "",
    val name: String = "",
    val phone: String = "",
    val location: String = "",
    val email: String = ""
): Parcelable
