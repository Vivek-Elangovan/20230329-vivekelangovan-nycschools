package com.jpmc.codechallenge.data.utils

import com.jpmc.codechallenge.model.School
import com.jpmc.codechallenge.model.SchoolScore
import com.jpmc.codechallenge.data.network.SchoolScoreResponse
import com.jpmc.codechallenge.data.network.SchoolsResponse

/**
 * Converts SchoolsResponse to School
 */
fun SchoolsResponse.toSchool() = School(
    dbn = dbn ?: "",
    name = schoolName ?: "",
    phone = phoneNumber ?: "",
    location = "$city, $stateCode",
    email = schoolEmail ?: ""
)

/**
 * Converts SchoolScoreResponse to SchoolScores
 */
fun SchoolScoreResponse.toSchoolScores() = SchoolScore(
    dbn = dbn ?: "",
    name = schoolName ?: "",
    avgMath = satMathAvgScore?.toDoubleOrNull() ?: 0.0,
    avgReading = satCriticalReadingAvgScore?.toDoubleOrNull() ?: 0.0,
    avgWriting = satWritingAvgScore?.toDoubleOrNull() ?: 0.0,
    students = numOfSatTestTakers?.toIntOrNull() ?: 0
)