package com.jpmc.codechallenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.codechallenge.data.repository.SchoolsRepository
import com.jpmc.codechallenge.model.School
import com.jpmc.codechallenge.data.utils.APIResult
import com.jpmc.codechallenge.model.SchoolScore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val schoolsRepository: SchoolsRepository,
    @Named("IO") private val dispatcherIo: CoroutineDispatcher
) : ViewModel() {

    private val _schoolsList = MutableLiveData<List<School>>()
    val schoolsList: LiveData<List<School>> get() = _schoolsList

    private val _details = MutableLiveData<SchoolScore>()
    val detailsLiveData: LiveData<SchoolScore> get() = _details

    private val _apiErrorMessage = MutableLiveData<String?>()
    val apiErrorMessage: LiveData<String?> get() = _apiErrorMessage

    /**
     * To get List of School Information
     */
    fun getSchools() {
        viewModelScope.launch(dispatcherIo) {
            when (val result = schoolsRepository.getSchools()) {
                is APIResult.Failure -> {
                    _apiErrorMessage.postValue(result.error.message)
                }
                is APIResult.Success -> {
                    _schoolsList.postValue(result.data as List<School>)
                }
            }

        }
    }

    /**
     * To get School Detail Information
     */
    fun getSchoolDetails(school: School) {
        viewModelScope.launch(dispatcherIo) {
            when (val result = schoolsRepository.getSchoolScores(school.dbn)) {
                is APIResult.Failure -> {
                    _apiErrorMessage.postValue(result.error.message)
                }
                is APIResult.Success -> {
                    result.data.firstOrNull().let { schoolScore ->
                        if (schoolScore != null) {
                            _details.postValue(schoolScore)
                        } else {
                            _apiErrorMessage.postValue("No Details Found")
                        }
                    }
                }
            }
        }
    }

}