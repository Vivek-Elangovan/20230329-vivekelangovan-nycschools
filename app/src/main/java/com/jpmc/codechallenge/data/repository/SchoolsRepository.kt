package com.jpmc.codechallenge.data.repository

import com.jpmc.codechallenge.model.School
import com.jpmc.codechallenge.model.SchoolScore
import com.jpmc.codechallenge.data.utils.APIResult

interface SchoolsRepository {
    suspend fun getSchools(): APIResult<List<School>>
    suspend fun getSchoolScores(dbn: String): APIResult<List<SchoolScore>>
}